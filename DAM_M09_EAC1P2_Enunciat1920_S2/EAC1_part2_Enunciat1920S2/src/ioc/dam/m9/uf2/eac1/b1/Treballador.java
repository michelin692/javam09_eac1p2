/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.dam.m9.uf2.eac1.b1;

/**
 *
 * @author Usuari
 */
public class Treballador {

	private String nom;

	// Constructor, getter y setter
        public Treballador(String nom) {            
            this.nom = nom;
        }

        public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

    
        
	public void processarOperacio(Client client, long timeStamp) {

		System.out.println("La Treballador " + this.nom + 
                " COMENÇA A PROCESSAR L'OPERACIÓ DEL CLIENT: " + client.getNom() + 
                " EN EL Temps: " + (System.currentTimeMillis() - timeStamp) / 1000 + "min");
                
                //for(int i = 0; i < client.getOperacions(); i++ ){
                    this.esperarXsegons(client.getOperacions());
                    System.out.println("Processat l'operació "+"->Temps: "+ (System.currentTimeMillis() - timeStamp) / 1000 + "seg");
                //}
                
                
	}


	private void esperarXsegons(int segons) {
		try {
			Thread.sleep(segons * 1000);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
	}

}