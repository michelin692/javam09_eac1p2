/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.dam.m9.uf2.eac1.b2;

import java.util.Random;

/**
 *
 * @author Usuari
 */
public class Principal {

	public static void main(String[] args) {
            
            int valor = 3;
            Random op = new Random();
            long tempsInicial = System.currentTimeMillis();
            Client client1 = new Client("Client 1", valor);
            valor = op.nextInt(7)+1;
            
            Client client2 = new Client("Client 2", valor);
            valor = op.nextInt(7)+1;
            
            BanquerThread banquer1 = new BanquerThread("Banquer/a 1", client1, tempsInicial);
            BanquerThread banquer2 = new BanquerThread("Banquer/a 2", client2, tempsInicial);
            //execucío del fills.
            banquer1.start();
            banquer2.start();
                    
	}
} 
