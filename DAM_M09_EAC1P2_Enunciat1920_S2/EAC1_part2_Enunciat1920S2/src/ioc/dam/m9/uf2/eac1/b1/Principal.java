/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.dam.m9.uf2.eac1.b1;

import java.util.Random;

/**
 *
 * @author Usuari
 */
public class Principal {

	public static void main(String[] args) {
            
            int valor = 3;
            Random op = new Random();
            long tempsInicial = System.currentTimeMillis();
            Client client1 = new Client("Client 1", valor);
            valor = op.nextInt(7)+1;
            
            Client client2 = new Client("Client 2", valor);
            valor = op.nextInt(7)+1;
            
            Client client3 = new Client("Client 3", valor);
            valor = op.nextInt(7)+1;
            Client client4 = new Client("Client 4", valor);
            valor = op.nextInt(7)+1;
            
            Treballador banquer1 = new Treballador("Banquer/a 1");
            banquer1.processarOperacio(client1, tempsInicial);
            
            Treballador banquer2 = new Treballador("Banquer/a 2");
            banquer2.processarOperacio(client2, tempsInicial);
            
            Treballador banquer3 = new Treballador("Banquer/a 3");
            banquer3.processarOperacio(client3, tempsInicial);
            
            Treballador banquer4 = new Treballador("Banquer/a 4");
             banquer4.processarOperacio(client4, tempsInicial);
            
        }
}