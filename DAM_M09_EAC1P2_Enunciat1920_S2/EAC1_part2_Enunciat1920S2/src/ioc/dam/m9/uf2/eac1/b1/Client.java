/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.dam.m9.uf2.eac1.b1;

/**
 *
 * @author Usuari
 */
public class Client {

	private String nom;
	private int Operacions;

	// Constructor, getter y setter

    public Client(String nom, int Operacions) {
        this.nom = nom;
        this.Operacions = Operacions;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getOperacions() {
        return Operacions;
    }

    public void setOperacions(int Operacions) {
        this.Operacions = Operacions;
    }

}
