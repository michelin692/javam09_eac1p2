package ioc.dam.m9.uf2.eac1.b3;

import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
public class GestioBanc {
    public static void main(String[] args) {

        int numCaixers=3; //introduïm el número de caixers

        Banc s= new Banc(numCaixers); //creem els caixers

        int numClients=20; //introduïm el número de clients.
        
        Client c[]= new Client [numClients]; //Creem els clients
        
        for (int i = 0; i < c.length; i++) {
            int j = new Random().nextInt(numCaixers);
           c[i] = new Client(i+1,s);
           c[i].start();
        }
        try {    
        for (int i= 0; i < c.length; i++){      
            c[i].join();    
        }   
    } catch (InterruptedException ex) {
        
        System.out.println("Fil principal interrumput.");  
    }   
   
        System.out.println("Banc buit.");   
        
        //System.out.println("Tiemps mig d'espera: " +    (Resultats.temps_espera / Resultats.clients_atessos) + " ms "  +" (fem la simulació que un seg és un min ->" + ((Resultats.temps_espera / Resultats.clients_atessos)/1000) + " min)");                 
    } 
        
        
        
       
    }



