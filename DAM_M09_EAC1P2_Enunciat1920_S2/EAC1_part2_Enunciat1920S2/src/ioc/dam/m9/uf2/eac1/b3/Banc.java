/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.dam.m9.uf2.eac1.b3;

import java.util.Random;

/**
 *
 * @author Usuari
 */
public class Banc {

    CaixerAutomatic[] caixer;
    int numCaixers;
    

    public Banc(int numCaixers) {
        this.numCaixers = numCaixers;
        caixer = new CaixerAutomatic[numCaixers];
        for (int i = 0; i < numCaixers; i++) {
            CaixerAutomatic caixerAu = new CaixerAutomatic(i + 1);
            caixer[i] = caixerAu;

        }
    }

    public CaixerAutomatic[] getCaixer() {
        return caixer;
    }

    public void setCaixer(CaixerAutomatic[] caixer) {
        this.caixer = caixer;
    }

    public int getNumCaixers() {
        return numCaixers;
    }

    public void setNumCaixers(int numCaixers) {
        this.numCaixers = numCaixers;
    }
    
    

    public void arribaClient(int numCli) {
        System.out.println("Entra al banc el client " + numCli);
    }

    public void passaACuaCaixerAutomatic(int numCli, int numCaixerAutomatic) {
        caixer[numCaixerAutomatic].passaACuaCaixer(numCli);


    }

    public void atendre(int numCaixerAutomatic) {
        caixer[numCaixerAutomatic].atendre();
        
    }

    public void surt(int numCaixerAutomatic) {
        caixer[numCaixerAutomatic].surt();
     
    }
}
