/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.dam.m9.uf2.eac1.b2;

/**
 *
 * @author Usuari
 */
public class BanquerThread extends Thread {

	private String nom;

	private Client client;

	private long initialTime;

	// Constructor, getter & setter

    public BanquerThread(String nom, Client client, long initialTime) {
        this.nom = nom;
        this.client = client;
        this.initialTime = initialTime;
    }
        

        public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

        public Client getClient() {
            return client;
        }

        public void setClient(Client client) {
            this.client = client;
        }

        public long getInitialTime() {
            return initialTime;
        }

        public void setInitialTime(long initialTime) {
            this.initialTime = initialTime;
        }

    
         
        
	@Override
	public void run() {
            System.out.println("El/La banquer/a " + this.nom + " Comença a Processar l'operació del CLIENT " + this.client.getNom() + " EN EL TEMPS: " 
            + (System.currentTimeMillis() - this.initialTime) / 1000 
            + "seg");
            this.esperarXsegons(this.client.getOperacio());
            System.out.println("Processat l'operació del client "+this.client.getNom()+" "+"->Temps: "+ (System.currentTimeMillis() - this.initialTime) / 1000 + "seg");
	}

	private void esperarXsegons(int segons) {
		try {
			Thread.sleep(segons * 1000);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
	}

}
