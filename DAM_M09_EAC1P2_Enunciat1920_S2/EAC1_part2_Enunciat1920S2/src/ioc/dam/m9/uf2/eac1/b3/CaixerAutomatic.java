/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.dam.m9.uf2.eac1.b3;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Usuari
 */
public class CaixerAutomatic {

    private static final int MAX_TIME = 1500;

    private final int numCaixerAutomatic;
    private boolean lliure = true;
    private Queue<Integer> q = new LinkedList();
    private int clientUtilitzant;

    public CaixerAutomatic(int id) {
        numCaixerAutomatic = id;

    }

    public synchronized void passaACuaCaixer(int numCli) {
        q.add(numCli);
    }

     public synchronized void atendre() {
         if(this.isLliure()){
             q.poll();
             this.setLliure(false);
         }else{
             try {
                 wait();
             } catch (InterruptedException ex) {
                 Logger.getLogger(CaixerAutomatic.class.getName()).log(Level.SEVERE, null, ex);
             }
         }
         
           
         
    }

    public synchronized void surt() {
        this.setLliure(true);
        notify();
    }

    public synchronized boolean isLliure() {
        return this.lliure;
    }

    public synchronized void setLliure(boolean lliure) {
        this.lliure = lliure;
    }

}